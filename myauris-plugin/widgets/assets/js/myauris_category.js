(function ($) {

    // alert('ok')
    // console.log($(document))
    console.log('myauris category')
    $('.slider').slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        dots: true,
        auto:true,
        infinite: true,
        cssEase: 'linear',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                },

            },
            {
                breakpoint: 426,
                settings: {
                    slidesToShow: 2
                },

            },
            {
                breakpoint: 1023,
                settings: {
                    slidesToShow: 3,
                },

            }
        ]
    });


})(jQuery);