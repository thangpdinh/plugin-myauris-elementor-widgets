<?php
/**
 * Date: 7/29/20
 * Time: 10:07 AM
 */

class Video_Post_Myauris extends \Elementor\Widget_Base {


	public function __construct( array $data = [], array $args = null ) {
		parent::__construct( $data, $args );
		wp_register_script( 'video-post-myauris', plugins_url( '/assets/video-myauris/js/video-myauris.js', __FILE__ ), [
			'elementor-frontend',
			'jquery',
			'html5lightbox'
		], '1.0.0', true );
		wp_register_style( 'video-post-myauris-style',
			plugins_url( '/assets/video-myauris/css/video-myauris.css', __FILE__ )
		);
	}


	public function get_style_depends() {
		return [ 'video-post-myauris-style' ];
	}

	public function get_script_depends() {
		return [
			'video-post-myauris'
		];
	}


	/**
	 * Get widget title.
	 *
	 * Retrieve Category Carousel widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'MyAuris Video', 'myauris' );
	}

	public function get_name() {
		return 'myauris_post_video';
	}

	public function get_icon() {
		return 'fa fa-video';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Category Carousel widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'basic' ];
	}

	/**
	 * Register Category Carousel widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'plugin-name' ),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'url',
			[
				'label'       => __( 'Nothing to do with this', 'myauris' ),
				'type'        => \Elementor\Controls_Manager::TEXT,
				'input_type'  => 'url',
				'placeholder' => __( 'https://your-link.com ', 'myauris' ),
			]
		);

		$this->end_controls_section();

	}

	public function get_youtube_id( $url ) {
		$video_id = '';
		if ( preg_match( '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/\s]{11})%i', $url, $match ) ) {
			$video_id = $match[1];
		}

		return $video_id;

	}

	/**
	 * Render Category Carousel widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		$posts       = get_posts();
		$template    = '<div class="video-myauris">%1$s</div>';
		$format_html = '<div class="video-item">
            <div class="head-item">
                <a class="html5lightbox"
                   href="%1$s"
                   data-width=800 data-height=400
                   title=""><img
                            src="%2$s"
                            alt="%6$s">
                </a>
            </div>
            <div class="body-item">
                <div class="title-item">
                    <a href="%7$s">
                        <h6>
                            %3$s
                        </h6>
                    </a>
                </div>
                <div class="meta-item">
                    <ul class="meta list-inline list-unstyled">
                        <li class="list-inline-item dt-published">
                            <i class="fa fa-clock"></i><span>%4$s</span>
                        </li>
                        <li class="list-inline-item">
                            <i class="fa fa-eye"></i><span>%5$s</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>';
		$res         = '';
		foreach ( $posts as $post ) {
			$youtube_link = '';
			if ( empty( $youtube_link ) ) {
				$youtube_link = 'https://www.youtube.com/watch?v=g6AHFrA1XBc';
			}
			$id_youtube    = $this->get_youtube_id( $youtube_link );
			$youtube_image = 'https://img.youtube.com/vi/' . $id_youtube . '/maxresdefault.jpg';
			if ( empty( $youtube_image ) ) {
				$youtube_image = 'https://img.youtube.com/vi/youtube_id/maxresdefault.jpg';
			}

			$viewed_count = pvc_get_post_views( $post->ID );;
			$res .= sprintf( $format_html, $youtube_link, $youtube_image, $post->post_title, $post->post_date, $viewed_count, 'post-image', get_permalink( $post ) );
		}
		printf( $template, $res );
		?>

		<?php
	}

}