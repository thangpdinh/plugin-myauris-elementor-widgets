<?php
/**
 * Date: 7/29/20
 * Time: 3:25 PM
 */

class Khachhang_Carousel extends \Elementor\Widget_Base {
	public function get_name() {
		return 'khachhang_carousel';
	}

	public function __construct( array $data = [], array $args = null ) {
		parent::__construct( $data, $args );
		wp_register_script( 'khachhang-carousel-myauris', plugins_url( '/assets/khachhang-carousel/js/khachhang-carousel.js', __FILE__ ), [
			'elementor-frontend',
			'jquery',
//			'html5lightbox'
		], '1.0.0', true );
		wp_register_style( 'khachhang-carousel-myauris-style',
			plugins_url( '/assets/khachhang-carousel/css/khachhang-carousel.css', __FILE__ )
		);
	}


	public function get_script_depends() {
		return [
			'khachhang-carousel-myauris'
		];
	}

	public function get_style_depends() {
		return [ 'khachhang-carousel-myauris-style' ];
	}


	/**
	 * Get widget title.
	 *
	 * Retrieve Category Carousel widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'MyAuris Khách Hàng Carousel', 'myauris' );
	}

	public function get_icon() {
		return 'fa fa-map';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Category Carousel widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'basic' ];
	}

	/**
	 * Register Category Carousel widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'plugin-name' ),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'url',
			[
				'label'       => __( 'Nothing to do with this', 'myauris' ),
				'type'        => \Elementor\Controls_Manager::TEXT,
				'input_type'  => 'url',
				'placeholder' => __( 'https://your-link.com ', 'myauris' ),
			]
		);

		$this->end_controls_section();

	}


	/**
	 * Render Category Carousel widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		$template    = '<div class="myauris-category">
                <div class="slider">%1$s</div></div>';
		$format_html = '';
		?>
        <div class="khachhang-carousel">
            <div class="khachhang-slider">
                <div class="item">
                    <div class="row">
                        <div class="col-md-4 d-block d-sm-none">
                            <div class="row ">
                                <div class="title">KHÁCH HÀNG</div>
                                <div class="meta">N. H. Như</div>
                            </div>
                        </div>
                        <div class="col-md-4 col-6 myauris-right image-before-after">
                            <div class="image-class">
                                <img src="https://kienthucrangsu.com/wp-content/uploads/2020/07/6487165e3b45c71b9e54.jpg"
                                     alt="">
                            </div>
                        </div>
                        <div class="col-md-4 col-6 image-before-after myauris-left d-block d-sm-none">
                            <img src="https://kienthucrangsu.com/wp-content/uploads/2020/07/8650a1898c9270cc2983.jpg"
                                 alt="">
                        </div>
                        <div class="col-md-4">
                            <div class="row d-none d-lg-block d-md-block">
                                <div class="title">KHÁCH HÀNG</div>
                                <div class="meta">N. H. Như</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-6 myauris-before myauris-right">
                                    <div class="before-title ">
                                        Trước
                                    </div>
                                    <div class="age">Tuổi: <span>20</span></div>
                                    <hr>
                                    <div class="status">
                                        Tình trạng răng:
                                        <div class="detail before">
                                            <ul>
                                                <li>
                                                    <span>Ố Vàng</span>
                                                </li>
                                                <li><span>Nhiễm Tetra</span></li>
                                                <li><span>Khấp khểnh</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="footer">
                                        Chỉ số hạnh phúc: <span>30%</span>
                                        Thời gian thực hiện: <span>0</span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-6 myauris-after">
                                    <div class="after-title">
                                        Sau
                                    </div>
                                    <div class="age">Tuổi: <span>20</span></div>
                                    <hr>
                                    <div class="status">
                                        Tình trạng răng:
                                        <div class="detail after">
                                            <ul>
                                                <li><span>Dòng sứ Arisa</span></li>
                                                <li><span>Phủ sứ 20 răng</span></li>
                                                <li><span class="empty"></span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="footer">
                                        <div class="text">Chỉ số hạnh phúc: <span>100%</span></div>
                                        <div class="text">Thời gian thực hiện: <span>0</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 myauris-left image-before-after d-none d-lg-block d-md-block">
                            <img src="https://kienthucrangsu.com/wp-content/uploads/2020/07/8650a1898c9270cc2983.jpg"
                                 alt="">
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                        <div class="col-md-4 d-block d-sm-none">
                            <div class="row ">
                                <div class="title">KHÁCH HÀNG</div>
                                <div class="meta">N. H. Như</div>
                            </div>
                        </div>
                        <div class="col-md-4 col-6 myauris-right image-before-after">
                            <div class="image-class">
                                <img src="https://kienthucrangsu.com/wp-content/uploads/2020/07/6487165e3b45c71b9e54.jpg"
                                     alt="">
                            </div>
                        </div>
                        <div class="col-md-4 col-6 image-before-after myauris-left d-block d-sm-none">
                            <img src="https://kienthucrangsu.com/wp-content/uploads/2020/07/8650a1898c9270cc2983.jpg"
                                 alt="">
                        </div>
                        <div class="col-md-4">
                            <div class="row d-none d-lg-block d-md-block">
                                <div class="title">KHÁCH HÀNG</div>
                                <div class="meta">N. H. Như</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-6 myauris-before myauris-right">
                                    <div class="before-title ">
                                        Trước
                                    </div>
                                    <div class="age">Tuổi: <span>20</span></div>
                                    <hr>
                                    <div class="status">
                                        Tình trạng răng:
                                        <div class="detail before">
                                            <ul>
                                                <li>
                                                    <span>Ố Vàng</span>
                                                </li>
                                                <li><span>Nhiễm Tetra</span></li>
                                                <li><span>Khấp khểnh</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="footer">
                                        Chỉ số hạnh phúc: <span>30%</span>
                                        Thời gian thực hiện: <span>0</span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-6 myauris-after">
                                    <div class="after-title">
                                        Sau
                                    </div>
                                    <div class="age">Tuổi: <span>20</span></div>
                                    <hr>
                                    <div class="status">
                                        Tình trạng răng:
                                        <div class="detail after">
                                            <ul>
                                                <li><span>Dòng sứ Arisa</span></li>
                                                <li><span>Phủ sứ 20 răng</span></li>
                                                <li><span class="empty"></span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="footer">
                                        <div class="text">Chỉ số hạnh phúc: <span>100%</span></div>
                                        <div class="text">Thời gian thực hiện: <span>0</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 myauris-left image-before-after d-none d-lg-block d-md-block">
                            <img src="https://kienthucrangsu.com/wp-content/uploads/2020/07/8650a1898c9270cc2983.jpg"
                                 alt="">
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                        <div class="col-md-4 d-block d-sm-none">
                            <div class="row ">
                                <div class="title">KHÁCH HÀNG</div>
                                <div class="meta">N. H. Như</div>
                            </div>
                        </div>
                        <div class="col-md-4 col-6 myauris-right image-before-after">
                            <div class="image-class">
                                <img src="https://kienthucrangsu.com/wp-content/uploads/2020/07/6487165e3b45c71b9e54.jpg"
                                     alt="">
                            </div>
                        </div>
                        <div class="col-md-4 col-6 image-before-after myauris-left d-block d-sm-none">
                            <img src="https://kienthucrangsu.com/wp-content/uploads/2020/07/8650a1898c9270cc2983.jpg"
                                 alt="">
                        </div>
                        <div class="col-md-4">
                            <div class="row d-none d-lg-block d-md-block">
                                <div class="title">KHÁCH HÀNG</div>
                                <div class="meta">N. H. Như</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-6 myauris-before myauris-right">
                                    <div class="before-title ">
                                        Trước
                                    </div>
                                    <div class="age">Tuổi: <span>20</span></div>
                                    <hr>
                                    <div class="status">
                                        Tình trạng răng:
                                        <div class="detail before">
                                            <ul>
                                                <li>
                                                    <span>Ố Vàng</span>
                                                </li>
                                                <li><span>Nhiễm Tetra</span></li>
                                                <li><span>Khấp khểnh</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="footer">
                                        Chỉ số hạnh phúc: <span>30%</span>
                                        Thời gian thực hiện: <span>0</span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-6 myauris-after">
                                    <div class="after-title">
                                        Sau
                                    </div>
                                    <div class="age">Tuổi: <span>20</span></div>
                                    <hr>
                                    <div class="status">
                                        Tình trạng răng:
                                        <div class="detail after">
                                            <ul>
                                                <li><span>Dòng sứ Arisa</span></li>
                                                <li><span>Phủ sứ 20 răng</span></li>
                                                <li><span class="empty"></span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="footer">
                                        <div class="text">Chỉ số hạnh phúc: <span>100%</span></div>
                                        <div class="text">Thời gian thực hiện: <span>0</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 myauris-left image-before-after d-none d-lg-block d-md-block">
                            <img src="https://kienthucrangsu.com/wp-content/uploads/2020/07/8650a1898c9270cc2983.jpg"
                                 alt="">
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                        <div class="col-md-4 d-block d-sm-none">
                            <div class="row ">
                                <div class="title">KHÁCH HÀNG</div>
                                <div class="meta">N. H. Như</div>
                            </div>
                        </div>
                        <div class="col-md-4 col-6 myauris-right image-before-after">
                            <div class="image-class">
                                <img src="https://kienthucrangsu.com/wp-content/uploads/2020/07/6487165e3b45c71b9e54.jpg"
                                     alt="">
                            </div>
                        </div>
                        <div class="col-md-4 col-6 image-before-after myauris-left d-block d-sm-none">
                            <img src="https://kienthucrangsu.com/wp-content/uploads/2020/07/8650a1898c9270cc2983.jpg"
                                 alt="">
                        </div>
                        <div class="col-md-4">
                            <div class="row d-none d-lg-block d-md-block">
                                <div class="title">KHÁCH HÀNG</div>
                                <div class="meta">N. H. Như</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-6 myauris-before myauris-right">
                                    <div class="before-title ">
                                        Trước
                                    </div>
                                    <div class="age">Tuổi: <span>20</span></div>
                                    <hr>
                                    <div class="status">
                                        Tình trạng răng:
                                        <div class="detail before">
                                            <ul>
                                                <li>
                                                    <span>Ố Vàng</span>
                                                </li>
                                                <li><span>Nhiễm Tetra</span></li>
                                                <li><span>Khấp khểnh</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="footer">
                                        Chỉ số hạnh phúc: <span>30%</span>
                                        Thời gian thực hiện: <span>0</span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-6 myauris-after">
                                    <div class="after-title">
                                        Sau
                                    </div>
                                    <div class="age">Tuổi: <span>20</span></div>
                                    <hr>
                                    <div class="status">
                                        Tình trạng răng:
                                        <div class="detail after">
                                            <ul>
                                                <li><span>Dòng sứ Arisa</span></li>
                                                <li><span>Phủ sứ 20 răng</span></li>
                                                <li><span class="empty"></span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="footer">
                                        <div class="text">Chỉ số hạnh phúc: <span>100%</span></div>
                                        <div class="text">Thời gian thực hiện: <span>0</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 myauris-left image-before-after d-none d-lg-block d-md-block">
                            <img src="https://kienthucrangsu.com/wp-content/uploads/2020/07/8650a1898c9270cc2983.jpg"
                                 alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<?php
	}
}

