<?php
/**
 * Date: 7/29/20
 * Time: 3:25 PM
 */

class Post_Mansory extends \Elementor\Widget_Base {
	public function get_name() {
		return 'post_mansory';
	}

	public function __construct( array $data = [], array $args = null ) {
		parent::__construct( $data, $args );
		wp_register_script( 'post-mansory-myauris', plugins_url( '/assets/post-mansory/js/post-mansory.js', __FILE__ ), [
			'elementor-frontend',
			'jquery',
//			'html5lightbox'
		], '1.0.0', true );
		wp_register_style( 'post-mansory-myauris-style',
			plugins_url( '/assets/post-mansory/css/post-mansory.css', __FILE__ ), [ 'bootstrap' ], false
		);
	}


	public function get_script_depends() {
		return [
			'post-mansory-myauris'
		];
	}

	public function get_style_depends() {
		return [ 'post-mansory-myauris-style' ];
	}


	/**
	 * Get widget title.
	 *
	 * Retrieve Category Carousel widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'MyAuris Post Mansory', 'myauris' );
	}

	public function get_icon() {
		return 'fa fa-book';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Category Carousel widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'basic' ];
	}

	/**
	 * Register Category Carousel widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'plugin-name' ),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'url',
			[
				'label'       => __( 'Nothing to do with this', 'myauris' ),
				'type'        => \Elementor\Controls_Manager::TEXT,
				'input_type'  => 'url',
				'placeholder' => __( 'https://your-link.com ', 'myauris' ),
			]
		);

		$this->end_controls_section();

	}


	/**
	 * Render Category Carousel widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		$template    = '<div class="post-mansonry">
        	<div class="post-mansonry-container post-mansory-slider d-flex flex-wrap">
                    %1$s
        	</div>
        </div>';
		$format_html = '<div class="mansonry-item col-md-6">
						<div class="row">
							<div class="col-md-6 col-6">
	                            <a class="mansonry-post__thumbnail__link" href="%2$s">
		                            <div class="mansonry-post__thumbnail"><img src="%1$s" class="attachment-medium size-medium" alt="">
		                            </div>
	                            </a>
                       		</div>
                       		 <div class="col-md-6 col-6">
                        		<div class="mansory-post__text">
		                            <h3 class="mansory-post__title">
		                                <a href="%2$s">
		                                    %3$s</a>
		                            </h3>
		                            <div class="mansonry-post__meta-data">
		                            %4$s
		                            </div>
		                            <div class="mansonry-post__excerpt">
		                                <p>%5$s</p>
		                            </div>
	                            </div>
                        	</div>
                        </div>
                    </div>
';
		$meta_box    = '<div class="meta-item">
                    <ul class="meta list-inline list-unstyled">
                        <li class="list-inline-item dt-published">
                            <i class="fa fa-clock"></i>%1$s
                        </li>
                        <li class="list-inline-item dt-clock">
                            <i class="fa fa-eye"></i>%2$s
                        </li>
                    </ul>
                </div>';

		$posts = get_posts( [
			'numberposts' => 4
		] );
		$res   = '';
		if ( ! empty( $posts ) ) {
			foreach ( $posts as $post ) {
				$viewed_count = pvc_get_post_views( $post->ID );;
				$meta_box_temp = sprintf( $meta_box, $post->post_date, $viewed_count );
				$image         = '';
				if ( has_post_thumbnail( $post->ID ) ) {
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
					if ( isset( $image[0] ) ) {
						$image = $image[0];
					}
				}
				$res .= sprintf( $format_html, $image, get_permalink( $post ), $post->post_title, $meta_box_temp, $post->post_content );
			}
		}
		?>
		<?php
		printf( $template, $res );

	}


}

